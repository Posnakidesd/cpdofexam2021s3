package com.agiletestingalliance;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

public class DurationTest extends TestCase {

    Duration duration = new Duration();

    @Test
    public void testDesc() {

        String actual = duration.dur();
        
        String expected = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
        Assert.assertEquals(expected, actual);
    }
}
